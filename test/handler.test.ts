import { describe, expect, it, vi } from 'vitest';
import { handler } from '../src/handler.js';
import { Env } from '../src/index.js';
import * as mail from '../src/mail.js';

const env: Env = {
	FRONT_ORIGINS: ['https://example.com'],
	DKIMPrivateKey: 'dummy',
	CSRFToken: 'test-csrf-token',
	FROM_ADDR: '',
	TO_ADDR: '',
	DKIM_DOMAIN: '',
};

describe('Worker', () => {
	describe('リクエスト内容のバリデーション', () => {
		describe('Origin', () => {
			it('origin が無いアクセスには 403 を返す', async () => {
				const req = new Request('http://example.com', {
					method: 'post',
				});
				const res = await handler(req, env);
				expect(res.status).toBe(403);
			});
			it('正しくない origin からのアクセスには 403 を返す', async () => {
				const req = new Request('http://example.com', {
					method: 'post',
					headers: {
						Origin: 'http://example.com',
					},
				});
				const res = await handler(req, env);
				expect(res.status).toBe(403);
			});
		});
		describe('X-Csrf', () => {
			it('X-Csrf ヘッダが無いアクセスには 403 を返す', async () => {
				const req = new Request('http://example.com', {
					method: 'post',
					headers: {
						Origin: 'https://example.com',
					},
				});
				const res = await handler(req, env);
				expect(res.status).toBe(403);
			});
			it('X-Csrf ヘッダが正しくないアクセスには 403 を返す', async () => {
				const req = new Request('http://example.com', {
					method: 'post',
					headers: {
						Origin: 'https://example.com',
						'X-Csrf': 'dummy',
					},
				});
				const res = await handler(req, env);
				expect(res.status).toBe(403);
			});
		});
		describe('データの中身', () => {
			it('データの中身が json でない場合は 400 を返す', async () => {
				const req = new Request('http://example.com', {
					method: 'post',
					headers: {
						Origin: 'https://example.com',
						'X-Csrf': 'test-csrf-token',
					},
					body: 'dummy',
				});
				const res = await handler(req, env);
				expect(res.status).toBe(400);
			});
			it('データの中身が所定の形式でない場合は 400 を返す', async () => {
				const req = new Request('http://example.com', {
					method: 'post',
					headers: {
						Origin: 'https://example.com',
						'X-Csrf': 'test-csrf-token',
					},
					body: JSON.stringify({ name: 'dummy' }),
				});
				const res = await handler(req, env);
				expect(res.status).toBe(400);
			});
		});
	});

	describe('メール送信', () => {
		it('メール送信できた場合は result: true を返す', async () => {
			const spy = vi.spyOn(mail, 'sendMail').mockResolvedValue({
				result: true,
				error: null,
			});
			const req = new Request('http://example.com', {
				method: 'post',
				headers: {
					Origin: 'https://example.com',
					'X-Csrf': 'test-csrf-token',
				},
				body: JSON.stringify({ name: '', email: '', content: 'test' }),
			});

			const res = await handler(req, env);
			expect(spy).toBeCalledTimes(1);
			expect(res.status).toBe(200);
			expect(await res.json()).toEqual({
				result: true,
				error: null,
			});
		});
		it('メール送信できなかった場合は result: false を返す', async () => {
			const spy = vi.spyOn(mail, 'sendMail').mockResolvedValue({
				result: false,
				error: 'Unauthorized',
			});
			const req = new Request('http://example.com', {
				method: 'post',
				headers: {
					Origin: 'https://example.com',
					'X-Csrf': 'test-csrf-token',
				},
				body: JSON.stringify({ name: '', email: '', content: 'test' }),
			});

			const res = await handler(req, env);
			expect(spy).toBeCalledTimes(1);
			expect(res.status).toBe(200);
			expect(await res.json()).toEqual({
				result: false,
				error: 'Unauthorized',
			});
		});
	});
});
