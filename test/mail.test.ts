import { beforeEach, describe, expect, it, vi } from 'vitest';
import { sendMail } from '../src/mail.js';
import { rest } from 'msw';
import { server } from './mocks/server.js';

describe('mail', () => {
	describe('sendMail', () => {
		describe('正常系', () => {
			const mockFn = vi.fn();
			beforeEach(() => {
				server.use(
					rest.post('https://api.mailchannels.net/tx/v1/send', (_, res, ctx) => {
						mockFn();
						return res(ctx.status(202));
					}),
				);
			});

			it('メールを送信できたとき result: true を返す', async () => {
				const result = await sendMail({
					dkimPrivateKey: 'dummy',
					mailContent: 'testContent',
					fromAddr: '',
					toAddr: '',
					dkimDomain: '',
				});
				expect(mockFn).toHaveBeenCalledTimes(1);
				expect(result).toEqual({
					result: true,
					error: null,
				});
			});
		});
		describe('異常系', () => {
			const mockFn = vi.fn();
			beforeEach(() => {
				server.use(
					rest.post('https://api.mailchannels.net/tx/v1/send', (_, res, ctx) => {
						mockFn();
						return res(ctx.status(401));
					}),
				);
			});

			it('メール送信に失敗したとき result: false を返す', async () => {
				const result = await sendMail({
					dkimPrivateKey: 'dummy',
					mailContent: 'testContent',
					fromAddr: '',
					toAddr: '',
					dkimDomain: '',
				});
				expect(mockFn).toHaveBeenCalledTimes(1);
				expect(result).toEqual({
					result: false,
					error: 'Unauthorized',
				});
			});
		});
	});
});
