# API for a contact form

問い合わせフォームから送信されてきた内容を自分のメールアドレスにメール送信する Cloudflare Worker です。

## 制限事項

MailChannels API を使っているので、ローカル環境ではメール送信できません。

## 前提条件

Cloudflare でドメイン管理済み。

## MailChannels API を使う準備

- DNSSEC を有効化する
  - データを参照する側が DNS からの応答データの偽装を検知できるようにする
  - ref. https://developers.cloudflare.com/dns/dnssec/#enable-dnssec
- SPFレコードを追加する
  - 正当な送信元メールサーバーを宣言する
  - ref. https://support.mailchannels.com/hc/en-us/articles/200262610-Set-up-SPF-Records
    - Type: TXT
    - Name: @
    - TTL: Auto
    - Content: `v=spf1 include:relay.mailchannels.net ~all`
- Domain Lockdown™レコードを追加する
  - 宣言した Cloudflare Worker からしかメール送信できないようにする
  - ref. https://support.mailchannels.com/hc/en-us/articles/16918954360845-Secure-your-domain-name-against-spoofing-with-Domain-Lockdown-
    - Type: TXT
    - Name: `_mailchannels`
    - TTL: Auto
    - Content: `v=mc1 cfid=example.workers.dev`
      - example.workers.dev は自分の worker 環境の Subdomain に置き換える
- DKIM Signature を追加する

  - 電子メールに電子署名を付加し送信者のドメインの認証を行えるようにする
  - ref. https://support.mailchannels.com/hc/en-us/articles/7122849237389-Adding-a-DKIM-Signature
    - 秘密鍵を作成
      ```bash
      openssl genrsa 2048 | tee priv_key.pem | openssl rsa -outform der | openssl base64 -A > priv_key.txt
      ```
    - 公開鍵を作成
      ```bash
      echo -e "v=DKIM1;p=" > dkim_record.txt && openssl rsa -in private_key.pem -pubout -outform der | openssl base64 -A >> dkim_record.txt
      ```
    - DKIM レコードを追加する
      - Type: TXT
      - Name: `mailchannels._domainkey`
      - TTL: Auto
      - Content: 先ほど生成したdkim_record.txtファイルの値（v=DKIM1; p=...）

- DMARCレコードを追加する

  - SPFおよびDKIMレコードと照合された後の電子メールの処理を決める
  - ref.
    - https://support.mailchannels.com/hc/en-us/articles/7122849237389-Adding-a-DKIM-Signature
    - Type: TXT
    - Name: `_dmarc`
    - TTL: Auto
    - Content: `v=DMARC1; p=reject; adkim=s; aspf=s; rua=mailto:test@example.com; ruf=mailto:test@example.com; pct=100; fo=1;`
      - `rua` と `ruf` はDMARCレポートの送信先なので自分のメールアドレスに置き換える

## deploy 準備

production 環境用のコマンドを書いています。

- 環境変数を設定
  - DKIMPrivateKey（DKIM Signature を追加するときに生成した priv_key.txt の中身。全環境で同じ値を設定する）
    ```bash
    cat priv_key.txt | pnpm wrangler secret put DKIMPrivateKey --env production
    ```
  - その他、以下を設定（example.dev.vars を参考に）
    - CSRFToken（CSRF 対策で付与する追加ヘッダの値）
    - FRONT_ORIGINS（CORS で許可する Origin）
    - FROM_ADDR（送信元メールアドレス。DKIM レコードを設定したドメインであること）
    - TO_ADDR（送信先メールアドレス）
    - DKIM_DOMAIN（DKIM レコードを設定したドメイン。全環境で同じ値を設定する）

## deploy

```bash
pnpm deploy:prod
```

# 問い合わせフォーム側

問い合わせフォームの例

```html
<div>
	<form id="contact-form" method="post" action="">
		<label>
			<p>お名前</p>
			<input name="name" type="text" />
		</label>
		<label>
			<p>メールアドレス</p>
			<input name="email" type="email" />
		</label>
		<label>
			<p>お問い合わせ内容（※必須）</p>
			<textarea name="content" required></textarea>
		</label>
		<button id="contact-form-submit" type="button">送信</button>
	</form>
	<p id="contact-form-response"></p>
</div>
```

`contact.js`

```js
const MAX_LENGTH = 5000;
const postFetch = () => {
	const errorText = document.getElementById('contact-form-response');
	errorText.textContent = '';

	const form = document.getElementById('contact-form');
	if (!form.checkValidity()) {
		form.reportValidity();
		return;
	}

	const formData = new FormData(form);
	for (let [_, value] of formData.entries()) {
		if (value.length > MAX_LENGTH) {
			errorText.textContent = `各入力項目は ${MAX_LENGTH} 文字以内で入力してください。`;
			return;
		}
	}

	fetch('https://contact-api-production.example.workers.dev', {
		method: 'POST',
		mode: 'cors',
		body: JSON.stringify(Object.fromEntries(formData.entries())),
		headers: {
			'Content-Type': 'application/json;charset=UTF-8',
			'X-Csrf': '6bed3c3f-1186-9562-5ae9-1edbb6f28240',
		},
	})
		.then((response) => {
			if (!response.ok) {
				errorText.textContent = '送信に失敗しました。';
				return;
			}
			return response.json();
		})
		.then((data) => {
			console.log(data);
			if (data.error !== null) {
				errorText.textContent = `正常に送信できませんでした。${data.error}`;
				return;
			}
			errorText.textContent = '送信しました。';
		})
		.catch((error) => {
			console.error(error);
			errorText.textContent = '送信に失敗しました。';
		});
};

document.getElementById('contact-form-submit').addEventListener('click', postFetch, false);
```

`<head>` 内で `contact.js` を読み込む。

```html
<script src="js/contact.js" type="module"></script>
```

## 補足

この API では `X-Csrf` ヘッダの値は固定値なので、問い合わせフォーム側の送信処理がクライアントサイドで動く場合は値が露出します。

しかし本 API では値は重要ではなく、異なるオリジンからリクエストが飛んでくることを前提として

- `Origin` ヘッダの検証
- 固有の HTTP ヘッダ（本 API では `X-Csrf` ヘッダ）が存在することの検証

の 2 点で CSRF 対策を行っています。

`X-Csrf` ヘッダの値の検証はおまけです。
