interface EmailAddress {
	email: string;
	name?: string;
}

interface Personalization {
	to: [EmailAddress, ...EmailAddress[]];
	from?: EmailAddress;
	dkim_domain?: string;
	dkim_private_key?: string;
	dkim_selector?: string;
	reply_to?: EmailAddress;
	cc?: EmailAddress[];
	bcc?: EmailAddress[];
	subject?: string;
	headers?: Record<string, string>;
}

interface ContentItem {
	type: string;
	value: string;
}

interface SendMailProps {
	dkimPrivateKey: string;
	mailContent: string;
	fromAddr: string;
	toAddr: string;
	dkimDomain: string;
}

export async function sendMail({ dkimPrivateKey, mailContent, fromAddr, toAddr, dkimDomain }: SendMailProps) {
	const fromEmailAddress: EmailAddress = {
		email: fromAddr,
		name: 'No Reply',
	};
	const personalization: Personalization = {
		to: [
			{
				email: toAddr,
			},
		],
		from: fromEmailAddress,
		dkim_domain: dkimDomain,
		dkim_selector: 'mailchannels',
		dkim_private_key: dkimPrivateKey,
	};
	const content: ContentItem = {
		type: 'text/plain',
		value: mailContent,
	};
	const payload = {
		personalizations: [personalization],
		from: fromEmailAddress,
		subject: 'お問い合わせがありました',
		content: [content],
	};

	const result = await fetch('https://api.mailchannels.net/tx/v1/send', {
		method: 'POST',
		headers: {
			'content-type': 'application/json',
		},
		body: JSON.stringify(payload),
	});
	return {
		result: result.ok,
		error: result.ok ? null : result.statusText,
	};
}
