import { Env } from './index.js';
import { sendMail } from './mail.js';

interface FormData {
	name: string;
	email: string;
	content: string;
}

export async function handler(request: Request, env: Env): Promise<Response> {
	const origin = request.headers.get('origin');
	if (origin === null || !env.FRONT_ORIGINS.includes(origin)) {
		return new Response(null, { status: 403 });
	}
	const headers: HeadersInit = {
		'Access-Control-Allow-Methods': 'GET,HEAD,POST,OPTIONS',
		'Access-Control-Allow-Headers': 'X-Csrf,Content-Type',
		'Access-Control-Max-Age': '86400',
		'Access-Control-Allow-Origin': origin,
		Vary: 'Origin',
	};
	switch (request.method.toUpperCase()) {
		case 'OPTIONS':
			return new Response(null, {
				headers,
			});
		case 'POST': {
			const xCSRF = request.headers.get('X-Csrf');
			if (xCSRF === null || xCSRF !== env.CSRFToken) {
				return new Response(null, { status: 403 });
			}
			const data = await request.json().catch(() => null);
			if (!isFormData(data)) {
				return new Response(null, { status: 400 });
			}
			try {
				const result = await sendMail({
					dkimPrivateKey: env.DKIMPrivateKey,
					mailContent: `名前: ${data.name}\nメールアドレス: ${data.email}\n内容: ${data.content}`,
					fromAddr: env.FROM_ADDR,
					toAddr: env.TO_ADDR,
					dkimDomain: env.DKIM_DOMAIN,
				});
				return new Response(JSON.stringify(result), {
					headers: {
						...headers,
						'content-type': 'application/json;charset=UTF-8',
					},
				});
			} catch (e) {
				return new Response(
					JSON.stringify({ result: false, error: typeof e === 'object' && e !== null && 'message' in e ? e.message : 'unknown error' }),
					{
						headers: {
							...headers,
							'content-type': 'application/json;charset=UTF-8',
						},
					},
				);
			}
		}
		case 'GET':
		case 'HEAD':
			return new Response();
		default:
			return new Response(null, { status: 405, statusText: 'Method Not Allowed' });
	}
}

function isFormData(data: unknown): data is FormData {
	return (
		typeof data === 'object' &&
		data !== null &&
		'name' in data &&
		typeof data['name'] === 'string' &&
		'email' in data &&
		typeof data['email'] === 'string' &&
		'content' in data &&
		typeof data['content'] === 'string'
	);
}
