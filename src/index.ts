import { handler } from './handler.js';

export interface Env {
	FRONT_ORIGINS: string[];
	DKIMPrivateKey: string;
	CSRFToken: string;
	FROM_ADDR: string;
	TO_ADDR: string;
	DKIM_DOMAIN: string;
}

export default {
	async fetch(request: Request, env: Env) {
		return await handler(request, env);
	},
};
